import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import Home from './pages/Home';
import About from './pages/About';
import { history } from './history';

function AppRouter(props) {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/" component={Home} {...props} />
        <Route path="/about" component={About} {...props} />
      </Switch>
    </ConnectedRouter>
  );
}

export default AppRouter;

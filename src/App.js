import './App.css';
import Header from './components/Header';
import MainRouter from './Router';
import './index.scss';

function App(props) {
  return (
    <div className="App">
      <Header />
      <MainRouter {...props} />
    </div>
  );
}

export default App;

import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { routerMiddleware } from 'connected-react-router';
import storage from 'redux-persist/lib/storage/session';
import logger from 'redux-logger';
import { history } from '../history';
import thunk from 'redux-thunk';
import reducers from './reducers';

const enhancers = [];
const middlewares = [thunk, routerMiddleware(history), logger];

const persistConfig = {
  key: 'root',
  storage,
  blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, reducers);
const composedEnhancers = compose(applyMiddleware(...middlewares), ...enhancers);

const store = createStore(persistedReducer, {}, composedEnhancers);
const persistor = persistStore(store);

export default store;
export { persistor };

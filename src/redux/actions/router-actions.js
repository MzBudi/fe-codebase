import { push, goBack, replace } from 'connected-react-router';
import store from '../store';

const { dispatch } = store;

export const navigateTo = (page, useReplace = false) => {
  if (useReplace) dispatch(replace(page));
  else dispatch(push(page));
};

export const navigateBack = () => {
  dispatch(goBack());
};

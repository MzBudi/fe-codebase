import { Navbar, Nav } from 'react-bootstrap';
import React from 'react';

function Header() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
      <Navbar.Collapse id="responsive-navbar-nav">
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Nav className="mr-auto" />
        <Nav>
          <Nav.Link href="#/">Home</Nav.Link>
          <Nav.Link href="#/about" eventKey={2}>
            About
          </Nav.Link>
          <Nav.Link href="#deets" eventKey={3}>
            Location
          </Nav.Link>
          <Nav.Link href="#memes" eventKey={4}>
            Dank memes
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default Header;
